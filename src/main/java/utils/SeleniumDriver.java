package utils;

import java.io.FileInputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class SeleniumDriver {

    private static SeleniumDriver seleniumDriver;
    private static WebDriver driver;
    public static Configuration config = new Configuration();
    public static FileInputStream fis;
    private static String browser;
    private static String hubURL;
    private static String baseURL;
    private static boolean headless; 
    
    private static String defaultWaitTime = config.getProperty("defaultWaitTime");

    private  SeleniumDriver() {
		
    	SeleniumDriver.baseURL = config.getProperty("url");
		SeleniumDriver.browser = config.getProperty("browser");
		
		SeleniumDriver.hubURL = config.getProperty("hubURL");
		
		SeleniumDriver.headless = Boolean.parseBoolean(config.getProperty("headless"));
    	
		if (Boolean.parseBoolean(config.getProperty("runOnSeleniumGrid"))){
	    	if (SeleniumDriver.browser.equals("firefox")){
	        	FirefoxOptions capability = new FirefoxOptions();
	        	capability.addArguments("--headless");
	            try {
	    			driver = new RemoteWebDriver(new URL(SeleniumDriver.hubURL), capability);
	    		} catch (MalformedURLException e) {
	    			e.printStackTrace();
	    		}
	        }
	    	else{
	        	ChromeOptions capability = new ChromeOptions();
	        	capability.addArguments("--headless");
	            try {
	    			driver = new RemoteWebDriver(new URL(SeleniumDriver.hubURL), capability);
	    		} catch (MalformedURLException e) {
	    			e.printStackTrace();
	    		}
	        }
		}else{
			if (SeleniumDriver.browser.equals("firefox")){
				WebDriverManager.firefoxdriver().setup();
				FirefoxOptions firefoxOptions = new FirefoxOptions();
				if (SeleniumDriver.headless) {
					firefoxOptions.addArguments("--window-size=1920x1080");
					firefoxOptions.addArguments("--disable-gpu");
					firefoxOptions.addArguments("--disable-extensions");
					firefoxOptions.addArguments("--start-maximized");
					firefoxOptions.addArguments("--headless");
					
				}
				driver = new FirefoxDriver();
			}
			else{
				WebDriverManager.chromedriver().setup();
				ChromeOptions chromeOptions = new ChromeOptions();
				if (SeleniumDriver.headless) {
					chromeOptions.addArguments("--window-size=1920x1080");
					chromeOptions.addArguments("--disable-gpu");
					chromeOptions.addArguments("--disable-extensions");
					chromeOptions.addArguments("--start-maximized");
					chromeOptions.addArguments("--headless");	
				}
				driver = new ChromeDriver(chromeOptions);
			}
			
			driver.manage().window().maximize();
		}
    	
        new WebDriverWait(driver, Integer.parseInt(config.getProperty("timeout")));
        driver.manage().timeouts().implicitlyWait(Integer.parseInt(config.getProperty("timeout")), TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(Integer.parseInt(config.getProperty("pageLoadTimeout")), TimeUnit.SECONDS);
        driver.getWindowHandle();
    }

    public static void openPage(String url) {
        driver.get(url);
    }

    public static WebDriver getDriver() {
        return driver;
    }
    
    public static String getBaseUrl() {
        return baseURL;
    }
    
    public static Configuration getConfig() {
        return config;
    }

    public static void setUpDriver() {
       if (seleniumDriver == null)
            seleniumDriver = new SeleniumDriver();
    }

    public static void tearDown() {
        if (driver != null) {
        	driver.close();
        	if(!SeleniumDriver.browser.equals("firefox")) {
            	driver.quit();
        	}	
        }
        seleniumDriver = null;
    }
    
    public static void waitForPageToLoad()
    {
    	try {
			Thread.sleep(Integer.parseInt(defaultWaitTime));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    }
    
    public static void waitForPageToLoad(int miliseconds)
    {
    	try {
			Thread.sleep(miliseconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    }
    
    public static boolean isElementPresentByCssLocator(String locator)
    {
    	try {
            driver.findElement(By.cssSelector(locator));
        } catch (NoSuchElementException e) {
            return false;
        }
        return true;
    }
    
    public static boolean isElementPresent(WebElement webElement) {
        try {
            boolean isPresent = webElement.isDisplayed();
            return isPresent;
        } catch (NoSuchElementException e) {
            return false;
        }   
    }
}
