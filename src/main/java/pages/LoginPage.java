package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utils.SeleniumDriver;

public class LoginPage {
	
	String url;
	
	@FindBy(css="#ssd > table > tbody > tr > td > div.login_big_box > div > div > a:nth-child(3)")
	public WebElement empleado_button;
	
	@FindBy(css="#id_empNameTip")
	public WebElement usernameTip_input;
	
	@FindBy(css="#id_empName")
	public WebElement username_input;
	
	@FindBy(css="#id_empPwdTip")
	public WebElement passwordTip_input;
	
	@FindBy(css="#id_empPwd")
	public WebElement password_input;
	
	@FindBy(css="#id_empLogin")
	public WebElement login_button;
	
	
	public  LoginPage()
	{
		PageFactory.initElements(SeleniumDriver.getDriver(), this);
		this.url = SeleniumDriver.getBaseUrl() + "accounts/login/?next=/data/index/";
	}
	
	public String getUrl()
	{
		return this.url;
		
	}
	
	public void selectEmpleados()
	{
		this.empleado_button.click();
		
	}
	
	public void usernameInput(String username)
	{
		this.usernameTip_input.click();
		this.username_input.sendKeys(username);
		
	}
	
	public void passwordInput(String password)
	{
		this.passwordTip_input.click();
		this.password_input.sendKeys(password);
		
	}
	
	public void login()
	{
		this.login_button.click();
		
	}
}
