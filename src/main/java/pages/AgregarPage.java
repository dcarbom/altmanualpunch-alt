package pages;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import utils.SeleniumDriver;

public class AgregarPage {

	String url;
	public static final String hhin=(SeleniumDriver.getConfig().getProperty("hhin") != null)?SeleniumDriver.getConfig().getProperty("hhin"):"08"; 
	public static final String mmin=(SeleniumDriver.getConfig().getProperty("mmin") != null)?SeleniumDriver.getConfig().getProperty("mmin"):"00"; 
	public static final String hhout=(SeleniumDriver.getConfig().getProperty("hhout") != null)?SeleniumDriver.getConfig().getProperty("hhout"):"17"; 
	public static final String mmout=(SeleniumDriver.getConfig().getProperty("mmout") != null)?SeleniumDriver.getConfig().getProperty("mmout"):"30"; 
	public static final String hhouteat=(SeleniumDriver.getConfig().getProperty("hhouteat") != null)?SeleniumDriver.getConfig().getProperty("hhouteat"):"14"; 
	public static final String mmouteat=(SeleniumDriver.getConfig().getProperty("mmouteat") != null)?SeleniumDriver.getConfig().getProperty("mmouteat"):"00"; 
	public static final String hhineat=(SeleniumDriver.getConfig().getProperty("hhineat") != null)?SeleniumDriver.getConfig().getProperty("hhineat"):"15"; 
	public static final String mmineat=(SeleniumDriver.getConfig().getProperty("mmineat") != null)?SeleniumDriver.getConfig().getProperty("mmineat"):"00"; 

	@FindBy(css = "#id_checktime")
	private WebElement horaChecada_input;

	@FindBy(css = "#calendar > div > div.foot > button.okBut")
	private WebElement OkHoraChecada_button;

	@FindBy(css = "#id_checktype")
	private WebElement estado_select;

	@FindBy(css = "#OK")
	private WebElement OkAgregar_button;

	@FindBy(css = "#Cancel")
	private WebElement CancelAgregar_button;

	public AgregarPage() {
		PageFactory.initElements(SeleniumDriver.getDriver(), this);
		this.url = SeleniumDriver.getBaseUrl() + "selfatt/page/selfatt/CheckExactReport/";
	}

	public String getUrl() {
		return this.url;

	}

	public String insertTime(String lastEntryTime) {
		this.horaChecada_input.click();
		boolean customTime = Boolean.parseBoolean(System.getProperty("customTime"));
		String hhString = "";
		String mmString = "";
		if (customTime) {
			hhString = System.getProperty("hh");
			mmString = System.getProperty("mm");
			if (hhString == null || hhString.equals("")) {
				Date date = new Date(System.currentTimeMillis());
				int hora = Integer.parseInt(new SimpleDateFormat("HH").format(date));
				int min = Integer.parseInt(new SimpleDateFormat("mm").format(date));
				double minDecimals = Double.valueOf(min) / 60.0;
				double time = hora + minDecimals;
				if (lastEntryTime.equals(hhout + ":" + mmout) && (time >= (Double.parseDouble(hhin) + ( Double.parseDouble(mmin) / 60.0 )))) {
					hhString = hhin;
					mmString = mmin;
				} else if (lastEntryTime.equals(hhin + ":" + mmin) && (time >= (Double.parseDouble(hhouteat) + ( Double.parseDouble(mmouteat) / 60.0 )))) {
					hhString = hhouteat;
					mmString = mmouteat;
				} else if (lastEntryTime.equals(hhouteat + ":" + mmouteat)) {
					if ((time >= (Double.parseDouble(hhin) + ( Double.parseDouble(mmin) / 60.0 ))) && (time >= (Double.parseDouble(hhouteat) + ( Double.parseDouble(mmouteat) / 60.0 )))) {
						hhString = hhin;
						mmString = mmin;
					} else if (time >= (Double.parseDouble(hhineat) + ( Double.parseDouble(mmineat) / 60.0 ))) {
						hhString = hhineat;
						mmString = mmineat;
					}
				} else if (lastEntryTime.equals(hhineat + ":" + mmineat) && (time >= (Double.parseDouble(hhout) + ( Double.parseDouble(mmout) / 60.0 )))) {
					hhString = hhout;
					mmString = mmout;
				} else {
					this.OkHoraChecada_button.click();
					return "ERROR";
				}
			}

			WebElement hh_input = SeleniumDriver.getDriver().findElement(By.cssSelector("input.hh"));
			WebElement mm_input = SeleniumDriver.getDriver().findElement(By.cssSelector("input.mm"));

			hh_input.clear();
			hh_input.sendKeys(hhString);
			mm_input.clear();
			mm_input.sendKeys(mmString);

			this.OkHoraChecada_button.click();
			return hhString + ":" + mmString;
		}

		this.OkHoraChecada_button.click();
		return "";
	}

	public String selectEstado(String lastEntryType) {
		Select selectEstado = new Select(estado_select);
		String customType = "";
		customType = System.getProperty("type");
		if (customType == null || customType.equals("")) {
			if (lastEntryType.equals("Salida")) {
				selectEstado.selectByValue("0");
				return "Entrada";
			} else {
				selectEstado.selectByValue("1");
				return "Salida";
			}
		} else {
			if (customType.equalsIgnoreCase("in")) {
				selectEstado.selectByValue("0");
				return "Entrada";
			} else {
				selectEstado.selectByValue("1");
				return "Salida";
			}
		}
	}

	public void aceptar_agregacion() {
		this.OkAgregar_button.click();
		// this.CancelAgregar_button.click();
	}

	public void cancelar_agregacion() {
		this.CancelAgregar_button.click();
	}

}
