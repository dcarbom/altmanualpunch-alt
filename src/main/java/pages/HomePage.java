package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utils.SeleniumDriver;

public class HomePage {
	
	String url;
	
	@FindBy(css="#AddManyCheckExact")
	private WebElement agregar_button;
	
	@FindBy(css="#id_checktime")
	private WebElement horaChecada_input;
	
	@FindBy(css="#calendar > div > div.foot > button.okBut")
	private WebElement OkHoraChecada_button;
	
	@FindBy(css="#id_checktype")
	private WebElement estado_select;
	
	@FindBy(css="iframe[src='/selfatt/att/CheckExact/_op_/AddManyCheckExact/']")
	private WebElement agregar_frame;
	
	@FindBy(css="#flex > tbody > tr:nth-child(1) > td:nth-child(13) > div")
	private WebElement lastEntryType_cell;
	
	@FindBy(css="#flex > tbody > tr:nth-child(1) > td:nth-child(12) > div")
	private WebElement lastEntryTime_cell;
	
	@FindBy(css="#flex > tbody > tr:nth-child(1) > td:nth-child(15) > div")
	private WebElement lastEntryInsertionTime_cell;
	
	@FindBy(css="iframe[src*='/selfatt/page/selfatt/CheckExactReport/']")
	private WebElement datatable_frame;
	
	public  HomePage()
	{
		PageFactory.initElements(SeleniumDriver.getDriver(), this);
		this.url = SeleniumDriver.getBaseUrl() + "selfatt/page/selfatt/CheckExactReport/";
	}
	
	public String getUrl()
	{
		return this.url;
		
	}
	
	public void agregar()
	{
		this.agregar_button.click();	
		SeleniumDriver.getDriver().switchTo().defaultContent();
	    SeleniumDriver.getDriver().switchTo().frame(agregar_frame);
	}
	
	public String getLastEntryInsertionTime()
	{	
	    String lastEntryInsertionTime = lastEntryInsertionTime_cell.getText();
	    return lastEntryInsertionTime;
	}
	
	public String getLastEntryTime()
	{	
	    String lastEntryTime = lastEntryTime_cell.getText();
	    return lastEntryTime;
	}
	
	public String getLastEntryType()
	{	
	    String lastEntryType = lastEntryType_cell.getText();
	    return lastEntryType;
	}
	
	public WebElement getDatatableFrame()
	{	
	    return this.datatable_frame;
	}
	
	public WebElement getAgregarFrame()
	{	
	    return this.agregar_frame;
	}
	
	public void printLastInsertion() {
		System.out.println("**************************************");
		System.out.println("Last insertion time:");
		System.out.println(this.getLastEntryInsertionTime());
		System.out.println("**************************************");
		System.out.println("Last entry:");
		System.out.println(this.getLastEntryTime());
		System.out.println(this.getLastEntryType());
		System.out.println("**************************************");
	}
	
}
