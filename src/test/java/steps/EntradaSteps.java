package steps;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.testng.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.AgregarPage;
import pages.HomePage;
import pages.LoginPage;
import utils.SeleniumDriver;

public class EntradaSteps {

	LoginPage loginPage = new LoginPage();
	HomePage homePage = new HomePage();
	AgregarPage agregarPage;
	Date newEntryDate;
	String newEntryTime;
	String lastEntryTime;
	String lastEntryType;
	String entryType;
	
	private static final String username = System.getProperty("usr");
	private static final String password = System.getProperty("pwd");
	
	@Given("^I am on the login page$")
	public void i_am_on_the_login_page() {
	    SeleniumDriver.openPage(this.loginPage.getUrl());
	    SeleniumDriver.waitForPageToLoad(3000);
	    String actualPage = SeleniumDriver.getDriver().getCurrentUrl();
		String expectedPage = this.loginPage.getUrl();
	    Assert.assertEquals(actualPage, expectedPage, "Error al entrar a la pagina de fichaje");
	}
	
	@When("^get into home page$")
	public void get_into_home_page() {
		this.loginPage.selectEmpleados();
		SeleniumDriver.waitForPageToLoad(2000);
		this.loginPage.usernameInput(EntradaSteps.username);
		this.loginPage.passwordInput(EntradaSteps.password);
		this.loginPage.login();
		SeleniumDriver.waitForPageToLoad(4000);
		String actualPage = SeleniumDriver.getDriver().getCurrentUrl();
		String expectedPage = this.homePage.getUrl();
	    Assert.assertEquals(actualPage, expectedPage, "Error en el login"); 
	}
	
	@And("^create a new registry$")
	public void create_a_new_registry() {
		SeleniumDriver.getDriver().switchTo().frame( this.homePage.getDatatableFrame() );
		this.lastEntryTime = this.homePage.getLastEntryTime();
		this.lastEntryType = this.homePage.getLastEntryType();
		SeleniumDriver.getDriver().switchTo().defaultContent();
		this.homePage.agregar();
	    this.agregarPage = new AgregarPage();
		this.newEntryTime = this.agregarPage.insertTime(this.lastEntryTime.substring(11, this.lastEntryTime.length()-3));
		if (this.newEntryTime.equals("ERROR")) {
			this.agregarPage.cancelar_agregacion();
			this.entryType = "ERROR";
		} else {
			this.entryType = this.agregarPage.selectEstado(this.lastEntryType);
			SeleniumDriver.waitForPageToLoad(2000);
			this.agregarPage.aceptar_agregacion();
		}
		
		this.newEntryDate = new Date();
	}
	
	@Then("^I have a new registry$")
	public void I_have_new_registry() {
		this.lastEntryTime = this.homePage.getLastEntryTime();
		String lastEntryInsertionTime = this.homePage.getLastEntryInsertionTime();
		this.lastEntryType = this.homePage.getLastEntryType();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm"); 
	    String newEntryDateFormatted = formatter.format(this.newEntryDate);
	    
	    this.homePage.printLastInsertion();
	    
	    Assert.assertNotEquals(this.entryType, "ERROR", "No se puede imputar la hora indicada");
		
		Assert.assertEquals(lastEntryInsertionTime.substring(0, lastEntryInsertionTime.length()-6), newEntryDateFormatted.substring(0, newEntryDateFormatted.length()-3)); 
	     
		Assert.assertEquals(this.lastEntryType, this.entryType);
		
		if (!this.newEntryTime.equals("")) {
			Assert.assertEquals(this.lastEntryTime.substring(11, this.lastEntryTime.length()-3), this.newEntryTime); 
		}
		
		
		
		
	}
}
