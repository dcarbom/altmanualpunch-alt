package runners;

import java.io.File;

import org.testng.annotations.BeforeClass;

import com.cucumber.listener.ExtentCucumberFormatter;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(
        plugin = {
        		"json:target/surefire-reports/cucumber-report.json",
        		"usage:target/cucumber-usage.json", 
        		"junit:target/surefire-reports/cucumber-report.xml",
        		"pretty", 
        		"html:target/positive/cucumber.html",
        		"com.cucumber.listener.ExtentCucumberFormatter"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = {}       
        )

public class TestRunner extends AbstractTestNGCucumberTests  {
	
	@BeforeClass
    public static void setup() {
		String fileName = System.getProperty("user.dir")+"/target/Extent_Reports/report.html";
		File newFile = new File(fileName);
		ExtentCucumberFormatter.initiateExtentCucumberFormatter(newFile,true);
        ExtentCucumberFormatter.loadConfig(new File("src/test/resources/extent-config.xml"));
    }
	
}
