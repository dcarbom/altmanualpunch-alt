## Run the tests suite
Maven should be installed.

In the root folder of the project, where pom.xml is located, run:

####To enter the current time:

~~~
mvn test -Dusr=<username> -Dpwd=<password> -DcustomTime=False
~~~


####To enter the next correct time:

~~~
mvn test -Dusr=<username> -Dpwd=<password> -DcustomTime=True
~~~

####To enter a custom time:

~~~
mvn test -Dusr=<username> -Dpwd=<password> -DcustomTime=True -Dhh=<HH> -Dmm=<mm> -Dtype=<in/out>
~~~

example to punch 08:00 Entrada:

~~~
mvn test -Dusr=<username> -Dpwd=<password> -DcustomTime=True -Dhh=08 -Dmm=00 -Dtype=in
~~~





